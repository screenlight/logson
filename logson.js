const fs = require('fs');
const path = require('path');
const readline = require('readline');
const Stream = require('stream');

function getLines(fileName) {
    let inStream = fs.createReadStream(fileName);
    let outStream = new Stream;

    return new Promise((resolve, reject)=> {
        let rl = readline.createInterface(inStream, outStream);

        inStream.on('error', reject);
        let lines = [];
        
        rl.on('line', function (line) {
            lines.push(line);
        });

        rl.on('error', reject)

        rl.on('close', function () {
            resolve(lines)
        });
    })
}

function writeData(prefix,folder,data,cb){
    var time = new Date();
    data.time = time.getTime();
	
	if ( !folder ) {
		console.log(data),
		cb();
		return;
    }

    let pad = (x) => ('00'+x).slice(-2);

    var dailyName = prefix
        +'-'+ time.getFullYear()
        +'.'+ pad(time.getMonth()+1)
        +'.'+ pad(time.getDate())
    ;
	
	var hourlyName = dailyName+'.'+pad(time.getHours());

    var json = JSON.stringify(data);
    
    var dailyFile = path.join(folder,dailyName+'.logson');
    var hourlyFile = path.join(folder,hourlyName+'.logson');

    function addData(data,add){
        if ( typeof data != typeof add ) return data;

        switch ( typeof data ) {
            case 'object':
                var obj = {};
                for( var key in data ) {
                    obj[key] = addData(data[key],add[key]);
                }
                return obj;

            case 'number':
                return data + add;

            default:
                return data;
        }
    }

    function updateDailyFile(){
        function appendHourlyData(){
            fs.appendFile(dailyFile, json+"\n",function(err){
                if (err) console.log(err);

                cb();
            });
        }

        getLines(dailyFile)
            .then(function(lines){
                var lastData = JSON.parse(lines.pop().trim());

                if ( new Date(data.time).getHours() != new Date(lastData.time).getHours() ) {
                    appendHourlyData();
                    return;
                }

                if ( !lastData.divisor ) lastData.divisor = 1;
                lastData.divisor++;

                var newData = addData(lastData,data);
                newData.time = lastData.time;

                lines.push(JSON.stringify(newData));
                lines.push('');

                fs.writeFile(dailyFile,lines.join("\n"),function(err){
                    if (err) console.log(err);

                    cb();
                });
            })
            .catch(function(err){
                appendHourlyData();
            })
        ;
    }

	fs.writeFile(path.join(folder,prefix+'-current.json'),json,function(err){
		fs.appendFile(hourlyFile, json+"\n", function(err) {
			if (err) {
				console.log(err);
			}
			
			updateDailyFile();
		});
	});
}

module.exports = {
    write:writeData,
};